#include "lexical_analyser.h"
#include "grammar_parser.h"
#include "sintatic_analyser.h"
#include "code_generator.h"

int main(int argc, char **argv){
  if(argc < 3){
    printf("Expected usage: /dist/main src/basic_grammar.wirth test/a.basic\n");
    return 1;
  }

  GrammarParser g(argv[1]);
  bool res = g.parse();
  if(DEBUG){
    printf("%s\n", res ? "SIM" : "NAO");
    g.print_all();
  }
  
  LexicalAnalyser LA(argv[2]);
  if(!LA.process()) LA.print_errors();
  else if(DEBUG) LA.print_output();

  SintaticAnalyser SA(g, LA.output);
  SA.process();

  if(DEBUG) printf("SA processed!\n");
  if(DEBUG) SA.print_tree();

  CodeGenerator CG(SA.ir);
  auto code = CG.generate();

  if(DEBUG) printf("\n-------|||||-------\n");
  for(int i=0;i<code.size();i++){
    printf("%s\n", code[i].c_str());
  }
  return 0;
}