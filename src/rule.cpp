#include "config.h"
#include "rule.h"

#include<iostream>
#include<algorithm>

using namespace std;

char Rule::err_buff[300] = {0};

int Rule::create_node(){
  vector<edgeType> emptyList;
  edges.push_back(emptyList);
  return edges.size() - 1;
}

bool Rule::create_edge(int from, int to, string s, bool is_terminal){
  #ifdef HANDLE_RULE_ERRORS
    if(edges.size() < max(from, to)){
      sprintf(this->err_buff, "ERRO: grafo tem %d arestas. Tentou criar aresta de %d a %d com %s.", from, to, s.c_str());
      this->errors.push_back(this->err_buff);
      return false;
    }
  #endif

  edges[from].push_back(edgeType(to, s, is_terminal));
  return true;
}

void Rule::upcase(string &s){
  for(int i=0;i<s.size();i++)
    if('a' <= s[i] && s[i] <= 'z')
      s[i] = s[i] - 'a' + 'A';
}

// constructor
Rule::Rule(){
  lval = "";
  errors.clear();

  current_position = 0;
  current_node = this->create_node();
  // we are opening an empty parenthesis in the beginning of every rule
  // it will allow us to handle pipes | that are not encapsulated by (), [] or {} 
  
  ControlElement sentinel;
  sentinel.control = '$';
  sentinel.automaton_node = 0;
  sentinel.position = 0;
  control_stack.push(sentinel);
}

bool Rule::add_terminal(const string &s){
  int from = this->current_node;
  int to = this->create_node();
  bool is_terminal = true;
  bool created_edge;

  created_edge = this->create_edge(from, to, s, is_terminal);

  if(created_edge) this->current_node = to;

  current_position++;
  return created_edge;
}

bool Rule::add_nonterminal(const string &s){
  int from = this->current_node;
  int to = this->create_node();
  bool is_terminal = false;
  bool created_edge = this->create_edge(from, to, s, is_terminal);

  if(created_edge) this->current_node = to;

  current_position++;
  return created_edge;
}

bool Rule::add_element(const string &terminal, const string &nonterminal){
  if(terminal == "" && nonterminal != ""){
    return this->add_nonterminal(nonterminal);
  }
  else if(terminal != "" && nonterminal == ""){
    return this->add_terminal(terminal);
  }
  else{
    this->errors.push_back("ERRO ao adicionar elementos " + terminal + " (terminal) " + nonterminal + " (nao terminal).");
    return false;
  }
}

bool Rule::add_lval(string s){
  this->lval = s;
  return true;
}

bool Rule::add_control(char c){
  if(c == '=') return true;
  if(c == '(' || c == '[' || c == '{'){
    ControlElement opening;
    opening.control = c;
    opening.automaton_node = current_node;
    opening.position = current_position;
    control_stack.push(opening);

    current_position++;
    return true;
  }
  if(c == '|'){
    int initial_node = control_stack.front().automaton_node;
    
    EndOfOptionElement e;
    e.automaton_node_begin = initial_node;
    e.automaton_node_end = current_node;
    e.position = current_position;
    
    end_of_option_stack.push(e);

    current_node = initial_node;
    return current_node != -1;
  }

  char front;

  #ifdef HANDLE_RULE_ERRORS
    if(control_stack.top == 0){
      this->errors.push_back("ERRO: Expressao mal-formada. Ha um ) extra na gramatica.");
      return false;
    }
    
    front = control_stack.front().control;
    if((front == '(' && c != ')') || \
       (front == '[' && c != ']') || \
       (front == '{' && c != '}')){
        sprintf(this->err_buff, "ERRO: Expressao mal-formada. Ha um %c fechando com um %c.", c, front);
        this->errors.push_back(this->err_buff);
        return false;
    }
  #endif

  int initial_node = control_stack.front().automaton_node;

  // point all options endings to the real end
  while(end_of_option_stack.top > 0 && end_of_option_stack.front().position > control_stack.front().position){
    this->create_edge(end_of_option_stack.front().automaton_node_end, current_node, "", true);
    end_of_option_stack.pop();
  }

  if(c == '.'){
    current_position++;
    return true;
  }

  if(c == ')'){
    //this->current_node = initial_node;
  }
  if(c == ']'){
    this->create_edge(initial_node, current_node, "", true);
    // this->current_node = initial_node;
  }
  if(c == '}'){
    auto phantom_node = this->create_node();
    this->create_edge(initial_node, phantom_node, "", true);
    this->create_edge(this->current_node , initial_node, "", true);
    // this->create_edge(this->current_node, phantom_node, "", true);

    this->current_node = phantom_node;
  }

  control_stack.pop();

  current_position++;
  return true;
}

void Rule::print_graph(){
  for(int from=0;from<edges.size();from++){
    cout << from << ": ";
    for(int i=0;i<edges[from].size();i++){
      string val = edges[from][i].val;
      if(val.size() == 0) val = "emptychain";

      string is_terminal = edges[from][i].is_terminal ? "terminal" : "nao-terminal";

      cout << "(" << edges[from][i].to << "," << \
                    val << "," << \
                    is_terminal << "); ";
    }
    cout << "\n";
  }
}