#include "config.h"
#include "lexical_analyser.h"

#include <iostream>
#include <fstream>

using namespace std;

char control_chars[] = {'{', '}', '[', ']', '(', ')', '$'};

LexicalAnalyser::LexicalAnalyser(string _filename){
  errors.clear();
  output.clear();
  filename = _filename;
}

bool LexicalAnalyser::is_control(char x){
  for(int i=0;control_chars[i] != '$';i++)
    if(control_chars[i] == x) return true;
  return false;
}

bool LexicalAnalyser::is_digit(char x){
  return '0' <= x && x <= '9';
}

bool LexicalAnalyser::is_letter(char x){
  return ('a' <= x && x <= 'z') || ('A' <= x && x <= 'Z');
}

bool LexicalAnalyser::is_digit_or_letter(char x){
  return is_digit(x) || is_letter(x);
}

bool LexicalAnalyser::is_plus_or_minus(char x){
  return x == '+' || x == '-';
}

bool LexicalAnalyser::is_math_operator(char x){
  return x == '+' || x == '-' || x == '*' || x == '/' || x == '%';
}

bool LexicalAnalyser::is_white(char x){
  return x == '\n' || x == '\t' || x == ' ';
}

bool LexicalAnalyser::is_doublequote(char x){
  return x == '"';
}

string LexicalAnalyser::process_identifier(int pos){
  string ans = "";
  for(int i=pos;i<input.size() && is_digit_or_letter(input[i]);i++)
    ans += input[i];
  return ans;
}

string LexicalAnalyser::process_number(int pos){
  string ans = "";
  bool comma = false;
  if(is_plus_or_minus(input[pos])) ans += input[pos++];
  while(pos<input.size() && is_digit(input[pos]))
    ans += input[pos++];
  if(input[pos] == '.'){
    if(pos+1 < input.size() && is_digit(input[pos+1]))
      ans += input[pos++];
    else return ans;
  }
  while(pos<input.size() && is_digit(input[pos]))
    ans += input[pos++];
  if(input[pos] == 'E'){
    if((pos+1 < input.size() && is_digit(input[pos+1])) || \
       (pos+2 < input.size() && is_plus_or_minus(input[pos+1]) && is_digit(input[pos+2])))
      ans += input[pos++];
    else return ans;

    if(is_plus_or_minus(input[pos]))
      ans += input[pos++];
    while(pos<input.size() && is_digit(input[pos]))
      ans += input[pos++];
  }

  return ans;
}

string LexicalAnalyser::process_rawstring(int pos){
  string ans = "";
  pos++; // skip opening doublequote
  while(pos < input.size() && input[pos] != '"'){
    ans += input[pos];
    pos++;
  }
  return ans;
}

// Why bother creating an automaton if we may use switch/case?
bool LexicalAnalyser::process(){
  // open program file
  ifstream program_file(this->filename);
  if(!program_file.is_open()){
    this->errors.push_back("Unable to open program file");
    return false;
  }

  // saves content into string
  // this allow us to have lookahead power for differing between <= and <
  char a, b, c;
  string a_to_s;
  while(program_file.get(c)) input += c;

  // process the char stream
  string content = "";
  Token t;
  int line_count = 0;
  bool last_was_number = false;
  for(int i=0;i<input.size();i++){
    a = input[i];
    a_to_s = ""; a_to_s += a;
    b = i+1 < input.size() ? input[i+1] : '$';
    
    if(a == '<' && b == '=') t = Token("operator", "<=");
    else if(a == '<' && b == '>') t = Token("operator", "<>");
    else if(a == '<') t = Token("operator", "<");
    else if(a == '>' && b == '=') t = Token("operator", ">");
    else if(a == '=') t = Token("operator", "=");
    else if(is_control(a)) t = Token("control", a_to_s);
    else if((is_plus_or_minus(a) && !last_was_number && is_digit(b)) || is_digit(a)){
      content = process_number(i);
      i += content.size() - 1;
      if(content.find(".") == string::npos)
        t = Token("int", content);
      else
        t = Token("num", content);
    }
    else if(is_math_operator(a)) t = Token("operator", a_to_s);
    else if(is_letter(a)){
      content = process_identifier(i);
      i += content.size() - 1;
      t = Token("identifier", content);
    }
    else if(is_white(a)){
      if(a == '\n') line_count++;
      last_was_number = false;
      continue;
    }
    else if(is_doublequote(a)){
      content = process_rawstring(i);
      i += content.size() + 1; // +2 for the enclosing doublequotes; -1 dut to for increment
      t = Token("rawstring", content);
    }
    else if(a == ','){
      content = ",";
      t = Token("separator", content);
    }
    else{
      char error_message[100];
      sprintf(error_message, "Erro de sintaxe na linha %d\n", line_count);
      errors.push_back(error_message);
      return false;
    }

    content = "";
    last_was_number = ((t.classification == "int") || (t.classification == "num"));
    output.push_back(t);

    // actually, removing comments should not be the lexical analser job
    // but it is far more practical to do it here
    if(t.content == "REM"){
      i++;
      while(input[i] != '\n') i++;
      line_count++;
    }
  }
  return true;
}

void LexicalAnalyser::print_output(){
  printf("Lexical Analyser output:\n");
  for(int i=0;i<output.size();i++){
    printf(" (%s: %s)\n", output[i].content.c_str(), output[i].classification.c_str());
  }
  printf("\n");
}

void LexicalAnalyser::print_errors(){
  printf("Lexical Analyser errors:\n");
  for(int i=0;i<errors.size();i++){
    printf("%s;", errors[i].c_str());
  }
  printf("\n");
}