var accumulators = new Array(16);
var variables = new Array(65536); // 2 ^ 16
var data_buffer = new Array(1024);
var code = new Array(65536); // 2 ^ 16
var __code_length;

var call_stack = new Array(2048);
var __call_stack_top;

var __accumulator_stack = new Array(2048);
var __accumulator_stack_top;

var __program_counter;
var __print;
var __current_variable;

var __data_buffer_size;
var __current_data;

var __label_to_address = {};

function sleep(timer){
  for(var i=0;i<timer;i++){}
}

function set_accumulator_with_const(destiny, value){
  accumulators[destiny] = value;
}

function set_accumulator_with_current_variable(destiny){
  accumulators[destiny] = variables[__current_variable];
}

function copy_accumulator(destiny, source){
  accumulators[destiny] = accumulators[source];
}

function set_current_variable(base, offset){
  __current_variable = base + accumulators[offset];
}

function assign_current_variable(source){
  variables[__current_variable] = accumulators[source];
}

function op_plus(destiny, op1, op2){
  accumulators[destiny] = accumulators[op1] + accumulators[op2];
}

function op_minus(destiny, op1, op2){
  accumulators[destiny] = accumulators[op1] - accumulators[op2];
}

function op_times(destiny, op1, op2){
  accumulators[destiny] = accumulators[op1] * accumulators[op2];
}

function op_division(destiny, op1, op2){
  accumulators[destiny] = accumulators[op1] / accumulators[op2];
}

function map_label_to_address(label, address){
  __label_to_address[label] = address;
}

function cmd_data(value){
  data_buffer[__data_buffer_size] = value;
  __data_buffer_size = __data_buffer_size + 1;
  __data_buffer_size = __data_buffer_size % 1024;
}

function cmd_read(){
  if(__current_data == __data_buffer_size){
    __current_data = __current_data % __data_buffer_size;
  }
  variables[__current_variable] = data_buffer[__current_data];
  __current_data = __current_data + 1;
}

function goto(address){
  __program_counter = address;
}

function goto_label(label){
  __program_counter = __label_to_address[label];
}

function goto_label_if(label, a1, a2, op_code){
  var condition = false;
  if(op_code == '0') condition = (accumulators[a1] <  accumulators[a2]);
  if(op_code == '1') condition = (accumulators[a1] <= accumulators[a2]);
  if(op_code == '2') condition = (accumulators[a1] == accumulators[a2]);
  if(op_code == '3') condition = (accumulators[a1] >= accumulators[a2]);
  if(op_code == '4') condition = (accumulators[a1] >  accumulators[a2]);
  if(op_code == '5') condition = (accumulators[a1] != accumulators[a2]);
  
  if(condition) __program_counter = __label_to_address[label];
  else __program_counter = __program_counter + 1;
}

function gosub_label(label){
  call_stack[__call_stack_top] = __program_counter;
  __call_stack_top = __call_stack_top + 1;
  __program_counter = __label_to_address[label];
}

function sub_return(){
  __call_stack_top = __call_stack_top - 1;
  __program_counter = call_stack[__call_stack_top] + 1;
}

function print(){
  // console.log may print trailing newline, therefore we use process.stdout.write
  if(__print === undefined) process.stdout.write("undefined");
  else process.stdout.write(__print.toString());
}

function set_print_as_accumulator(source){
  __print = accumulators[source];
}

function set_print_as_string(value){
  __print = value;
}

function push_accumulator(source){
  __accumulator_stack[__accumulator_stack_top] = accumulators[source];
  __accumulator_stack_top = __accumulator_stack_top + 1;
}

function pop_accumulator(destiny){
  __accumulator_stack_top = __accumulator_stack_top - 1;
  accumulators[destiny] = __accumulator_stack[__accumulator_stack_top];
}

function halt_machine(){
  process.stdout.write("Program finished executing.\n");
}

function show(){
  process.stdout.write("accumulators:")
  for(var i=0;i<10;i++){
    process.stdout.write(" (" + i.toString() + " " + accumulators[i] + ")");
  }
  process.stdout.write("\n");
}

// function show_vars(){
//  process.stdout.write("variables:")
//  for(var i=0;i<32;i++){
//   if(variables[i] === undefined) continue;
//    process.stdout.write(" (" + i.toString() + " " + variables[i] + ")");
//  }
//  process.stdout.write("\n"); 
// }

function show_var(){
  console.log("curr var is at: " + __current_variable + " with value of " + variables[__current_variable]);
}

function execute(filename){
  // initialize
  __call_stack_top = 0;
  __accumulator_stack_top = 0;
  __current_data = 0;
  __data_buffer_size = 0;
  __program_counter = 0;

  var fs = require('fs');
  var fileContent = fs.readFileSync(filename).toString();
  code = fileContent.split("\n");
  __code_length = code.length;
  // initialized

  var isJumpInstruction = function(instruction){
    return instruction.startsWith("goto") || instruction.startsWith("gosub") || instruction.startsWith("sub_return");
  }

  while(__program_counter < __code_length){
    // if(255 <= __program_counter && __program_counter <= 287){
    //   console.log("Program counter: " + __program_counter);
    //   show();
    // }
    // console.log("program counter is: " + __program_counter);
    // sleep(1E2);

    var instruction = code[__program_counter];
    eval(code[__program_counter]);
    if(code[__program_counter] == "halt_machine()") return;
    if(!isJumpInstruction(instruction)) __program_counter = __program_counter + 1;
  }
}

execute(process.argv[2]);