#include "code_generator.h"

#include <sstream>

using namespace std;

int s_to_i(string x){
  int ans = 0;
  for(int i=0;i<x.size();i++){
    if(i == 0 && (x[i] == '+' || x[i] == '-')) continue;
    ans = (10*ans) + (x[i] - '0');
  }
  if(x[0] == '-') ans *= -1;
  return ans;
}

string i_to_s(int x){
  string ans = "";
  do{
    ans += ((x%10) + '0');
    x /= 10;
  } while(x > 0);
  for(int i=0;i<ans.size()/2;i++) {
    char tmp = ans[i];
    ans[i] = ans[ans.size() - 1 -i];
    ans[ans.size() - 1 -i] = tmp;
  }
  return ans;
}

double power_of_10(int n){
  double p10 = 1.0;
  int n_positive = n < 0 ? -n : n;
  for(int i=0;i<n_positive;i++) p10 *= 10.0;
  return n < 0 ? 1.0 / p10 : p10;
}

// convert -12.5E-4 to -0.000125
string static_double_evaluation(string x){
  string ans = "";
  double curr = 0;
  int p10e = 0, p10b = 0;
  bool negative = false, negative_p10 = false;
  bool saw_digit = false;
  bool saw_comma = false;
  bool saw_power = false;
  bool is_digit;
  for(int i=0;i<x.size();i++){
    if(!saw_digit && x[i] == '-') negative = !negative;

    is_digit = ('0' <= x[i] && x[i] <= '9');

    saw_digit |= is_digit;
    if(x[i] == '.') saw_comma = true;
    if(x[i] == 'E') saw_power = true;

    if(x[i] == '-' && saw_power) negative_p10 = !negative_p10;

    if(!is_digit) continue;

    if(!saw_power) curr = 10.0*curr + (x[i] - '0');
    if(!saw_power && saw_comma) p10e--;
    if(saw_power) p10b = 10*p10b + (x[i] - '0');
  }
  if(negative_p10) p10b = -p10b;
  curr = curr * power_of_10(p10b + p10e);

  if(negative) curr = -curr;

  std::ostringstream stream;
  stream << curr;
  ans = stream.str();

  return ans;
}

CodeGenerator::CodeGenerator(IRTree _ir) {
  ir = _ir;

  label_to_instruction_address.clear();

  variable_to_address.clear();
  variable_to_dims.clear();
  variable_to_size.clear();
  last_variable_address = 0;

  function_to_label.clear();
  function_to_parameter.clear();

  hashcode_to_label.clear();
  hashcode_to_from_expression_size.clear();
  hashcode_to_step_expression_size.clear();

  operation_to_op_code.clear();
  operation_to_op_code["<"]  = 0;
  operation_to_op_code["<="] = 1;
  operation_to_op_code["="]  = 2;
  operation_to_op_code[">="] = 3;
  operation_to_op_code[">"]  = 4;
  operation_to_op_code["<>"] = 5;
}

string CodeGenerator::create_hashcode(string base){
  return "hbegin__"+base+"__" + i_to_s(hashcode_to_label.size()) + "__hend";
}

Code CodeGenerator::solve(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];
  auto v = node.edge_val;

  if(v == "Program") return solve_program(node_idx);
  if(v == "BStatement") return solve_bstatement(node_idx);
  if(v == "Assign") return solve_assign(node_idx);
  if(v == "Var") return solve_var(node_idx);
  if(v == "Exp") return solve_exp(node_idx);
  if(v == "Eb") return solve_eb(node_idx);
  if(v == "Predef") return solve_predef(node_idx);
  if(v == "Read") return solve_read(node_idx);
  if(v == "Data") return solve_data(node_idx);
  if(v == "Print") return solve_print(node_idx);
  if(v == "Pitem") return solve_pitem(node_idx);
  if(v == "Goto") return solve_goto(node_idx);
  if(v == "If") return solve_if(node_idx);
  if(v == "For") return solve_for(node_idx);
  if(v == "Next") return solve_next(node_idx);
  if(v == "Dim") return solve_dim(node_idx);
  if(v == "Def") return solve_def(node_idx);
  if(v == "Gosub") return solve_gosub(node_idx);
  if(v == "Return") return solve_return(node_idx);
  if(v == "Remark") return solve_remark(node_idx);
  if(v == "End") return solve_end(node_idx);

  errors.push_back("UNKNOWN SYMBOL '" + v + "'\n");
  return ans;
}

Code CodeGenerator::solve_program(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  // generate code
  for(int i=0;i<node.sons.size();i++){
    int son_idx = node.sons[i];

    auto statement_code = solve_bstatement(son_idx);
    for(int k=0;k<statement_code.size();k++){
      code.push_back(statement_code[k]);
    }
  }

  // write the mappings from label to address before the code
  int mapping_instructions_length = label_to_instruction_address.size();
  for(auto it=label_to_instruction_address.begin();it!=label_to_instruction_address.end();++it){
    int label = it->first;
    int address = it->second + mapping_instructions_length;
    ans.push_back("map_label_to_address("+i_to_s(label)+","+i_to_s(address)+")");
  }

  for(int i=0;i<code.size();i++) ans.push_back(code[i]);

  // replace hashcodes
  map <string, string> hashcode_replacement;
  for(auto it=hashcode_to_label.begin();it!=hashcode_to_label.end();++it){
    string hashcode = it->first;
    int label = s_to_i(it->second);
    int address = label_to_instruction_address[label] + mapping_instructions_length;
    if(hashcode.find("__next__") != string::npos){
      auto label_it = label_to_instruction_address.find(label);
      int next_label = (++label_it)->first;

      hashcode_replacement[hashcode] = i_to_s(next_label);
    }
    else if(hashcode.find("__step__") != string::npos){
      int from_expression_size = hashcode_to_from_expression_size[hashcode];
      hashcode_replacement[hashcode] = i_to_s(address + from_expression_size + 5);
      // 5 is the number of commands between from_expression_code and the goto-comparison command (incluing this goto)
    }
    else if(hashcode.find("__comparison__") != string::npos){
      int from_expression_size = hashcode_to_from_expression_size[hashcode];
      int step_expression_size = hashcode_to_step_expression_size[hashcode];
      hashcode_replacement[hashcode] = i_to_s(address + from_expression_size + step_expression_size + 5 + 5); 
      // 5 is the number of commands between from_expression_code and the goto-comparison command (incluing this goto)
      // 5 is the number of commands between step_expression_code and the to_expression_code
    }
  }

  for(int i=0;i<ans.size();i++){
    if(ans[i].find("hbegin__") != string::npos){
      int hash_begin = ans[i].find("hbegin__");
      int hash_end = ans[i].find("__hend") + 6; /* 6 is the size of __hend */
      string hashcode = ans[i].substr(hash_begin, hash_end - hash_begin);
      ans[i].replace(hash_begin, hashcode.size(), hashcode_replacement[hashcode]);
    }
  }

  // copy ans to code
  code.clear();
  code = ans;

  return ans;
}

Code CodeGenerator::solve_bstatement(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  int label = s_to_i(ir.nodes[node.sons[0]].t.content);
  if(DEBUG) printf("Solving statement %d LABEL %d\n", node_idx, label);

  auto statement_code = solve(node.sons[1]);
  for(int i=0;i<statement_code.size();i++) ans.push_back(statement_code[i]);

  label_to_instruction_address[label] = code.size();

  return ans; 
}

Code CodeGenerator::solve_assign(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];
  
  auto expression_code = solve_exp(node.sons[3]);
  for(int i=0;i<expression_code.size();i++) ans.push_back(expression_code[i]);
  ans.push_back("copy_accumulator(9,0)");

  auto variable_code = solve(node.sons[1]);
  for(int i=0;i<variable_code.size();i++) ans.push_back(variable_code[i]);

  ans.push_back("assign_current_variable(9)");

  return ans;
}

Code CodeGenerator::solve_var(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  string var_name;
  vector <int> dimensions;

  auto leftmost_son = ir.nodes[node.sons[0]];
  var_name = leftmost_son.t.content;

  if(variable_to_address.count(var_name) == 0){
    // do a quick allocation
    vector <int> dimensions;
    int var_size = 1;
    variable_to_dims[var_name] = dimensions;
    variable_to_size[var_name] = var_size;
    variable_to_address[var_name] = last_variable_address;
    last_variable_address = last_variable_address + var_size;
  }

  int block_size = variable_to_size[var_name], count = 0;
  ans.push_back("set_accumulator_with_const(7,0)");
  ans.push_back("push_accumulator(7)");
  for(int i=2;i<node.sons.size()-1;i++){
    int son_idx = node.sons[i];
    auto son = ir.nodes[son_idx];

    if(son.t.content == ",") continue;

    block_size /= variable_to_dims[var_name][count];
    // now block_size contains the size of the remaining dmensions

    auto dimension_code = solve_exp(son_idx);
    for(int k=0;k<dimension_code.size();k++) ans.push_back(dimension_code[k]);
    // the parameter regarding (count+1)-th dimension is now at acc. 0

    ans.push_back("set_accumulator_with_const(8,"+i_to_s(block_size)+")");
    ans.push_back("op_times(6,0,8)");
    // 6 = EXPRESSION * PARTIAL_DIMENSION
    // 7 = SUM OF SIXES (i.e. the offset)
    ans.push_back("pop_accumulator(7)");
    ans.push_back("op_plus(7,7,6)");
    ans.push_back("push_accumulator(7)");

    count++;
  }
  ans.push_back("pop_accumulator(7)");

  string base = i_to_s(variable_to_address[var_name]);
  string offset = "7"; // accumulator n. 7
  ans.push_back("set_current_variable("+base+","+offset+")");

  return ans;
}

Code CodeGenerator::solve_exp(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  bool saw_first_eb = false; // as the grammar allows this aberration: ++-+-- Eb * Eb - Eb
  bool negative_expression = (ir.nodes[node.sons[0]].t.content == "-");

  string operation = "";

  for(int i=0;i<node.sons.size();i++){
    int son_idx = node.sons[i];
    auto son = ir.nodes[son_idx];

    if(son.edge_val == "Eb"){
      auto eb_code = solve_eb(son_idx);
      for(int k=0;k<eb_code.size();k++) ans.push_back(eb_code[k]);

      if(operation == "+") ans.push_back("op_plus(0,1,0)");
      if(operation == "-") ans.push_back("op_minus(0,1,0)");
      if(operation == "*") ans.push_back("op_times(0,1,0)");
      if(operation == "/") ans.push_back("op_division(0,1,0)");

      ans.push_back("copy_accumulator(1,0)");
      saw_first_eb = true;
    }
    else if(saw_first_eb) operation = son.t.content;
  }

  if(negative_expression){
    ans.push_back("set_accumulator_with_const(0,-1)");
    ans.push_back("op_times(0,1,0)");
  }

  return ans;
}

Code CodeGenerator::solve_eb(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  auto leftmost_son = ir.nodes[node.sons[0]];

  if(leftmost_son.t.content == "("){
    auto expression_code = solve_exp(node.sons[1]);
    for(int i=0;i<expression_code.size();i++) ans.push_back(expression_code[i]);
  }
  else if(leftmost_son.edge_val == "num"){
    ans.push_back("set_accumulator_with_const(0," + static_double_evaluation(leftmost_son.t.content) + ")");
  }
  else if(leftmost_son.edge_val == "int"){
    ans.push_back("set_accumulator_with_const(0," + static_double_evaluation(leftmost_son.t.content) + ")");
  }
  else if(leftmost_son.edge_val == "Var"){
    auto var_code = solve_var(node.sons[0]);
    for(int i=0;i<var_code.size();i++) ans.push_back(var_code[i]);
    ans.push_back("set_accumulator_with_current_variable(0)");
  }
  else if(leftmost_son.edge_val == "FN"){
    string function_name = ir.nodes[node.sons[1]].t.content;
    auto tmp = ir.nodes[node.sons[2]].t.content;
    if(tmp != "(") function_name += tmp;

    for(int i=0;i<node.sons.size();i++){
      int son_idx = node.sons[i];
      auto son = ir.nodes[son_idx];
      if(son.edge_val == "Exp"){
        auto expression_code = solve_exp(son_idx);
        for(int i=0;i<expression_code.size();i++) ans.push_back(expression_code[i]);

        string subroutine_label = i_to_s(function_to_label[function_name]);
        ans.push_back("gosub_label("+subroutine_label+")");
        break;
      }
    }
  }
  return ans;
}

Code CodeGenerator::solve_predef(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  // WILL NOT BE IMPLEMENTED!

  return ans;
}

Code CodeGenerator::solve_read(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  for(int i=1;i<node.sons.size();i++){
    int son_idx = node.sons[i]; 
    auto son = ir.nodes[son_idx];

    if(son.t.content == ",") continue;

    auto var_code = solve_var(son_idx);
    for(int k=0;k<var_code.size();k++) ans.push_back(var_code[k]);
    ans.push_back("cmd_read()");
  }

  return ans;
}

Code CodeGenerator::solve_data(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  for(int i=1;i<node.sons.size();i++){
    int son_idx = node.sons[i]; 
    auto son = ir.nodes[son_idx];

    if(son.t.content == ",") continue;

    ans.push_back("cmd_data("+static_double_evaluation(son.t.content)+")");
  }

  return ans;
}

Code CodeGenerator::solve_print(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  for(int i=0;i<node.sons.size();i++){
    int son_idx = node.sons[i];
    auto son = ir.nodes[son_idx];

    if(son.edge_val == "Pitem"){
      auto pitem_code = solve_pitem(son_idx);
      for(int k=0;k<pitem_code.size();k++) ans.push_back(pitem_code[k]);
      ans.push_back("print()");
    }
  }

  return ans;
}

Code CodeGenerator::solve_pitem(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  for(int i=0;i<node.sons.size();i++){
    int son_idx = node.sons[i];
    auto son = ir.nodes[son_idx];

    if(son.edge_val == "Exp"){
      auto expression_code = solve_exp(son_idx);
      for(int k=0;k<expression_code.size();k++) ans.push_back(expression_code[k]);
      ans.push_back("set_print_as_accumulator(0)");
    }
    else if(son.edge_val == "rawstring"){
      ans.push_back("set_print_as_string(\"" + son.t.content + "\")");
    }
  }

  return ans;
}

Code CodeGenerator::solve_goto(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  string label = ir.nodes[node.sons[node.sons.size() - 1]].t.content;

  ans.push_back("goto_label(" + label + ")");

  return ans;
}

Code CodeGenerator::solve_if(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];
  Code expression_code;

  expression_code = solve_exp(node.sons[1]);
  for(int i=0;i<expression_code.size();i++) ans.push_back(expression_code[i]);
  ans.push_back("copy_accumulator(4,0)");

  auto comparison_operator = ir.nodes[node.sons[2]].t.content;
  int op_code = operation_to_op_code[comparison_operator];

  expression_code = solve_exp(node.sons[3]);
  for(int i=0;i<expression_code.size();i++) ans.push_back(expression_code[i]);
  ans.push_back("copy_accumulator(5,0)");

  string label = ir.nodes[node.sons[5]].t.content;

  ans.push_back("goto_label_if("+label+",4,5,"+i_to_s(op_code)+")");

  return ans;
}

Code CodeGenerator::solve_for(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];
  string op_code = i_to_s(operation_to_op_code[">="]);
  string comparison_hashcode, step_hashcode, next_hashcode;
  Code from_expression_code, to_expression_code, step_expression_code;

  string for_variable = ir.nodes[node.sons[1]].t.content;
  auto tmp = ir.nodes[node.sons[2]].t.content;
  if(tmp != "=") for_variable += tmp;
  string base = i_to_s(variable_to_address[for_variable]);

  int from_idx, to_idx, equal_idx, step_idx = -1;
  for(int i=0;i<node.sons.size();i++){
    auto sons_content = ir.nodes[node.sons[i]].t.content;
    if(sons_content == "=") equal_idx = node.sons[i];
    if(sons_content == "TO") from_idx = node.sons[i-1];
    if(sons_content == "TO") to_idx = node.sons[i+1];
    if(sons_content == "STEP") step_idx = node.sons[i+1];
  }

  auto bstatement_node = ir.nodes[ir.T[node_idx]];
  string for_label = ir.nodes[bstatement_node.sons[0]].t.content;

  from_expression_code = solve_exp(from_idx);
  to_expression_code = solve_exp(to_idx);
  if(step_idx == -1){
    step_expression_code.push_back("set_accumulator_with_const(0,1)");
  } else {
    step_expression_code = solve_exp(step_idx);
  }

  step_hashcode = create_hashcode("step");
  comparison_hashcode = create_hashcode("comparison");
  next_hashcode = create_hashcode("next");

  hashcode_to_label[step_hashcode] = for_label;
  hashcode_to_label[comparison_hashcode] = for_label;

  hashcode_to_from_expression_size[step_hashcode] = from_expression_code.size();
  hashcode_to_from_expression_size[comparison_hashcode] = from_expression_code.size();

  hashcode_to_step_expression_size[step_hashcode] = step_expression_code.size();
  hashcode_to_step_expression_size[comparison_hashcode] = step_expression_code.size();

  for(int i=0;i<from_expression_code.size();i++) ans.push_back(from_expression_code[i]);
  // now the from is at acc 0.
  // pick for_variable
  ans.push_back("set_accumulator_with_const(2,0)");
  ans.push_back("set_current_variable(" + base + ",2)"); // offset on 1. accumulator
  // set the variable as the from value (acc 0.)
  ans.push_back("assign_current_variable(0)");
  // copy it to acc 3. (acc 3. will (later) also contain the variable+step value, and it is used for the comparison)
  ans.push_back("set_accumulator_with_current_variable(3)");
  ans.push_back("goto("+comparison_hashcode+")");
  for(int i=0;i<step_expression_code.size();i++) ans.push_back(step_expression_code[i]);
  // now the step is at acc 0.
  // pick for_variable
  ans.push_back("set_accumulator_with_const(2,0)");
  ans.push_back("set_current_variable(" + base + ",2)"); // offset on 1. accumulator  
  // set this val into acc 1.
  ans.push_back("set_accumulator_with_current_variable(1)");
  // sum the variable (acc 1.) with step (acc 0.) and store it into acc 3. and the variable itself
  ans.push_back("op_plus(3,0,1)");
  ans.push_back("assign_current_variable(3)");
  for(int i=0;i<to_expression_code.size();i++) ans.push_back(to_expression_code[i]);
  ans.push_back("goto_label_if("+next_hashcode+",3,0,"+op_code+")");

  for_stack.push(ForContextElement(for_variable, step_hashcode, comparison_hashcode, next_hashcode));

  return ans;
}

Code CodeGenerator::solve_next(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  string next_var = ir.nodes[node.sons[1]].t.content;
  auto tmp = ir.nodes[node.sons[2]].t.content;
  if(tmp != "=") next_var += tmp;

  auto for_context = for_stack.front(); for_stack.pop();
  ans.push_back("goto(" + for_context.step_hashcode + ")");

  auto bstatement_node = ir.nodes[ir.T[node_idx]];
  string label = ir.nodes[bstatement_node.sons[0]].t.content;
  hashcode_to_label[for_context.next_hashcode] = label;

  return ans;
}

Code CodeGenerator::solve_dim(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  string var_name;
  vector <int> dimensions;
  int k, var_size = 1;

  for(int i=1;i<node.sons.size();){
    int son_idx = node.sons[i];
    auto son = ir.nodes[son_idx];

    var_name = son.t.content;

    var_size = 1;
    dimensions.clear();
    for(k=i+2;true;k++){
      auto kth_son = ir.nodes[node.sons[k]];

      if(kth_son.t.content == ",") continue;
      if(kth_son.t.content == ")") break;

      int dimension_length = s_to_i(kth_son.t.content);
      var_size *= dimension_length;
      dimensions.push_back(dimension_length);
    }

    variable_to_dims[var_name] = dimensions;
    variable_to_size[var_name] = var_size;
    variable_to_address[var_name] = last_variable_address;

    last_variable_address = last_variable_address + var_size;

    i = k + 2;
  }

  return ans;
}

Code CodeGenerator::solve_def(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  // 0- DEF 1- FN 2- LETTER 3- OPENING PARENTHESIS 4- PARAMETER
  string function_name = ir.nodes[node.sons[2]].t.content;

  string parameter_name = ir.nodes[node.sons[4]].t.content;
  // check if parameter is letter+digit kind of variable
  auto tmp = ir.nodes[node.sons[5]].t.content;
  if(tmp != ")") parameter_name += tmp;

  function_to_parameter[function_name] = parameter_name;

  auto expression_code = solve_exp(node.sons[node.sons.size()-1]);
  
  auto bstatement_node = ir.nodes[ir.T[node_idx]];
  int subroutine_label = s_to_i(ir.nodes[bstatement_node.sons[0]].t.content);
  function_to_label[function_name] = subroutine_label;

  // salva contexto da variavel
  // the programmer should "declare" variables with LET before using DEF FN
  // otherwise compiler may not behave properly - and we don't care, because this issue was not specified before
  string base = i_to_s(variable_to_address[parameter_name]);
  ans.push_back("set_accumulator_with_const(1,0)");
  ans.push_back("set_current_variable("+base+",1)"); // offset of 1. accumulator
  ans.push_back("set_accumulator_with_current_variable(1)");
  // seta variavel como acumulador
  ans.push_back("assign_current_variable(0)");
  // evalua expressao (resultado no 0. acumulador)
  for(int i=0;i<expression_code.size();i++) ans.push_back(expression_code[i]);
  // reseta contexto da variavel
  ans.push_back("assign_current_variable(1)");
  ans.push_back("sub_return()");

  return ans;
}

Code CodeGenerator::solve_gosub(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  string label = ir.nodes[node.sons[1]].t.content;

  ans.push_back("gosub_label("+label+")");

  return ans;
}

Code CodeGenerator::solve_return(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  ans.push_back("sub_return()");

  return ans; 
}

Code CodeGenerator::solve_remark(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  return ans; 
}

Code CodeGenerator::solve_end(int node_idx){
  Code ans;
  auto node = ir.nodes[node_idx];

  ans.push_back("halt_machine()");

  return ans; 
}

Code CodeGenerator::generate(){
  return solve(0);
}