#include "ir_tree.h"

using namespace std;

IRTree::IRTree() {
  nodes.clear();
  T.clear();

  IRTreeNode root;
  root.edge_val = "Program";
  nodes.push_back(root);
  T.push_back(0);
  current_node = 0;
}

void IRTree::pointToParent(){
  current_node = T[current_node];
}

void IRTree::createNode(Token t, string edge_val){
  IRTreeNode new_node(t, edge_val);

  nodes.push_back(new_node);
  T.push_back(0);
  
  // create edges and move current pointer
  int last = nodes.size() - 1;

  nodes[current_node].sons.push_back(last);
  T[last] = current_node;

  current_node = last;
}

void IRTree::print(){
  for(int i=0;i<nodes.size();i++){
    auto node = nodes[i];
    printf("Node %d: %s (%s %s)\n", i, node.edge_val.c_str(), node.t.content.c_str(), node.t.classification.c_str());
    printf("  %d sons:", node.sons.size());
    for(int k=0;k<node.sons.size();k++) printf(" %d", node.sons[k]);
    printf("\n");
    printf("  %d sons:", node.sons.size());
    for(int k=0;k<node.sons.size();k++) printf(" %s( %s %s )   ", nodes[node.sons[k]].edge_val.c_str(), nodes[node.sons[k]].t.content.c_str(), nodes[node.sons[k]].t.classification.c_str());
    printf("\n");
    printf("\n");
  }
}