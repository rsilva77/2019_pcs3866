/*
Premises:
- Double-quote is special
- Line feeds
- Nor double-quotes nor line feeds should be described by the grammar as special characters
- No quote may appear between quotes (i.e. we are not implementing quote-escaping)
*/

#include "config.h"
#include "grammar_parser.h"

#include <iostream>
#include <fstream>

bool GrammarParser::is_wirth_control(char &c){
  return c == '(' || c == ')' || \
         c == '[' || c == ']' || \
         c == '{' || c == '}' || \
         c == '|' || c == '=' || c == '.';
}

bool GrammarParser::is_white(char &c){
  return c == ' ' || c == '\n' || \
         c == '\t' || c == EOF;
}

void GrammarParser::fill_rule_index_for(){
  rule_index_for.clear();
  for(int i=0;i<rules.size();i++) rule_index_for[rules[i].lval] = i;
}
/*
Reads Wirth grammar described in filename
May return false on error
*/
bool GrammarParser::parse(){
  // open grammar file
  ifstream grammar_file(this->filename);
  if(!grammar_file.is_open()){
    this->errors.push_back("Unable to open file");
    return false;
  }

  // process file as stream
  char c;
  string terminal = "", nonterminal = "";
  bool between_quotes = false;
  bool found_lval = false;
  Rule *rule = new Rule();
  while(grammar_file.get(c) && c != EOF){
    if(c == '"'){
      between_quotes = !between_quotes;
      if(terminal != "" || nonterminal != ""){
        rule->add_element(terminal, nonterminal);
        terminal = nonterminal = "";
      }
    }
    else if(between_quotes){
      terminal += c;
    }
    else{
      if(c == '='){
        rule->add_lval(nonterminal);
        nonterminal = "";
        found_lval = true;
      }
      else if(GrammarParser::is_wirth_control(c)){
        if(terminal != "" || nonterminal != ""){
          rule->add_element(terminal, nonterminal);
          terminal = nonterminal = "";
        }
        rule->add_control(c);

        if(c == '.'){
          this->rules.push_back(*rule);

          rule = new Rule();
          terminal = nonterminal = "";
          between_quotes = false;
          found_lval = false;
        }
      }
      else if(!GrammarParser::is_white(c)){
        nonterminal += c;
      }
      else if(found_lval && (terminal != "" || nonterminal != "")){
        rule->add_element(terminal, nonterminal);
        terminal = nonterminal = "";
      }
    }
  }

  fill_rule_index_for();
  if(raise_if_left_recursion_present()) return false;
  fill_follow();
  
  return true;
}

bool GrammarParser::raise_if_left_recursion_present(){
  bool has_left_recursion = false;
  for(int i=0;i<rules.size();i++){
    for(int k=0;k<rules[i].edges[0].size();k++){
      if(rules[i].edges[0][k].val == rules[i].lval){
        errors.push_back("Error: Grammar has left-recursive terminal: " + rules[i].lval + "\n");
        has_left_recursion = true;
      }
    }
  }
  return has_left_recursion;
}

void GrammarParser::print_all(){
  printf("Grammar has %d rules.\n", rules.size());
  for(int i=0;i<rules.size();i++){
    printf("Rule #%d: %s\n", i, rules[i].lval.c_str());
    rules[i].print_graph();

    printf("---- Follow -----\n");
    for(int j=0;j<MAX_FOLLOW;j++){
      printf("%d: ", j);
      for(auto it=follow[j][i].begin();it!=follow[j][i].end();++it){
        printf("[");
        for(auto iit=it->begin();iit!=it->end();++iit)
          printf("%s ", iit->content.c_str());
        printf("] ; ");
      }
      printf("\n");
    }
    printf("\n\n");
  }
}

void GrammarParser::fill_follow(){
  for(int i=0;i<MAX_FOLLOW;i++)
    for(int j=0;j<MAX_RULES;j++)
      follow[i][j].clear();

  for(int i=0;i<MAX_FOLLOW;i++){
    for(int j=0;j<rules.size();j++){
      vector <int> a, b;
      _dfs_depth = 0;
      follow[i][j] = dfs(j, 0, 0, i, a, b);
    }
  }
}

set <sequenceOfTerminals> GrammarParser::dfs(int rule_idx, int rule_node, int lookahead, int max_loohakead, vector <int> back_rule_idx, vector <int> back_rule_node){
  set <sequenceOfTerminals> ans;
  _dfs_depth++;
  if(_dfs_depth >= _DFS_DEPTH_LIMIT) {
    _dfs_depth--;
    return ans; 
  }

  for(int i=0;i<rules[rule_idx].edges[rule_node].size();i++){
    edgeType e = rules[rule_idx].edges[rule_node][i];
    if(e.is_terminal){
      int new_lookahead = lookahead;
      if(e.val != "") new_lookahead += 1;

      if(new_lookahead > max_loohakead) continue;

      set <sequenceOfTerminals> next = dfs(rule_idx, e.to, new_lookahead, max_loohakead, back_rule_idx, back_rule_node);

      if(next.empty() && e.val != ""){
        sequenceOfTerminals s;
        s.push_back(Token(e.val));
        ans.insert(s);
      }
      else{
        while(!next.empty()){
          sequenceOfTerminals s = *next.begin();
          next.erase(next.begin());

          if(e.val != "") s.insert(s.begin(), Token(e.val));

          ans.insert(s);
        }
      }
    }
    else {
      back_rule_idx.push_back(rule_idx);
      back_rule_node.push_back(e.to);
      set <sequenceOfTerminals> next = dfs(rule_index_for[e.val], 0, lookahead, max_loohakead, back_rule_idx, back_rule_node);
      back_rule_node.pop_back();
      back_rule_idx.pop_back();

      while(!next.empty()){
        ans.insert(*next.begin());
        next.erase(next.begin());
      }
    }
  }

  // jump back if its the last node fro automaton
  if(rule_node == rules[rule_idx].edges.size() - 1 && back_rule_idx.size() > 0){
    int return_idx = back_rule_idx[back_rule_idx.size() - 1];
    int return_node = back_rule_node[back_rule_node.size() - 1];
    back_rule_node.pop_back();
    back_rule_idx.pop_back();
    set <sequenceOfTerminals> next = dfs(return_idx, return_node, lookahead, max_loohakead, back_rule_idx, back_rule_node);

    while(!next.empty()){
      ans.insert(*next.begin());
      next.erase(*next.begin());
    }
  }

  _dfs_depth--;
  return ans;
}