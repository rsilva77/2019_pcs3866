#include "config.h"
#include "sintatic_analyser.h"

#include "stack.h"

#include <utility>

using namespace std;

bool SintaticAnalyser::canTraverseTerminalEdgeAsLetterDigit(Token a, edgeType edge){
  if(edge.val == "letter" && a.classification == "identifier" && a.content.size() == 1){
    return true;
  }
  if(edge.val == "letter" && a.classification == "identifier" && a.content.size() == 2){
    bool hasDigitEdge = false;
    for(int i=0;i<grammar.rules[rule_ptr].edges[edge.to].size();i++){
      auto secondEdge = grammar.rules[rule_ptr].edges[edge.to][i];
      if(secondEdge.val == "digit"){
        hasDigitEdge = true;
        digitEdge = secondEdge;
        break;
      }
    }

    return hasDigitEdge;
  }
  return false;
}

bool SintaticAnalyser::canTraverseNonTerminalEdgeAsLetterDigit(Token a, edgeType edge){
  if(a.classification == "identifier" && a.content.size() == 2){
    sequenceOfTerminals nxt;
    string tmp;
    tmp = ""; tmp += a.content[0]; nxt.push_back(Token(tmp));
    tmp = ""; tmp += a.content[1]; nxt.push_back(Token(tmp));
    return grammar.follow[2][grammar.rule_index_for[edge.val]].count(nxt) > 0;
  }
  return false;
}

bool SintaticAnalyser::canTraverse(Token a, edgeType edge, int lookahead){
  if(edge.is_terminal && edge.val == "") return true;

  if(edge.is_terminal || edge.val == "letter"){
    if(a.to_edge_val() == edge.val && lookahead == 1) return true;
    if(canTraverseTerminalEdgeAsLetterDigit(a, edge)){ // sets double_traverse_destination
      double_traverse = true;
      return true;
    }
    return false;
  }

  if(lookahead == 2 && canTraverseNonTerminalEdgeAsLetterDigit(a, edge)) return true;

  if(input_ptr+lookahead-1 >= input.size()) return false; // cant look beyond last token

  sequenceOfTerminals nxt;
  for(int i=1;i<=lookahead;i++){
    nxt.push_back(Token(input[input_ptr+i-1].to_edge_val()));
  }
  return grammar.follow[lookahead][grammar.rule_index_for[edge.val]].count(nxt) > 0;
}

vector <int> SintaticAnalyser::getPossibleEdges(){
  vector <int> ans;
  Token curr = input[input_ptr];
  Rule rule = grammar.rules[rule_ptr];
  for(int emptychain_only=0;emptychain_only<=1;emptychain_only++){
    double_traverse = false;
    ans.clear();

    for(int lookahead=1;lookahead<MAX_FOLLOW;lookahead++){
      for(int i=0;i<rule.edges[rule_node_ptr].size();i++){
        edgeType edge = rule.edges[rule_node_ptr][i];

        if(emptychain_only ^ edge.val == "") continue;
        if(canTraverse(curr, edge, lookahead)){
          ans.push_back(i);
        }
      }

      if(ans.size() > 0) break;
    }

    if(ans.size() == 1) break;
  }
  return ans;
}

bool SintaticAnalyser::process(){
  Stack <StackElement> stack;

  vector <int> indexes_candidate_edges; // for debug purposes

  if(DEBUG) printf("Input size is %d\n", input.size());
  input_ptr = 0;
  rule_ptr = 0;
  rule_node_ptr = 0;
  while(input_ptr<input.size()){
    Token curr = input[input_ptr];
    Rule rule = grammar.rules[rule_ptr];
    if(DEBUG) printf("Processing input ptr %d:  %s %s at %d %d\n", input_ptr, curr.content.c_str(), curr.classification.c_str(), rule_ptr, rule_node_ptr);

    indexes_candidate_edges = getPossibleEdges();

    if(DEBUG) printf("Got %d possible edges\n", indexes_candidate_edges.size());

    if(indexes_candidate_edges.size() == 0){
      string error_message = "Erro linha X";
      errors.push_back(error_message);
      input_ptr++;
      continue; // panic mode - this token gets ignored!
    }
    if(indexes_candidate_edges.size() >= 2){
      string error_message = "Erro: Multiplas possiveis acoes (LL(k)?)\n";
      errors.push_back(error_message);
      input_ptr++;
      continue; // panic mode - this token gets ignored!
    }

    if(DEBUG) printf("No errors so far\n");

    // then we have one edge we should follow...
    edgeType edge = rule.edges[rule_node_ptr][indexes_candidate_edges[0]];
    if(DEBUG) printf("This edge is %s %s to %d  double_traverse %s\n", edge.val.c_str(), edge.is_terminal ? "Terminal" : "Nonterminal", edge.to, double_traverse ? "SIM" : "NAO");

    if(edge.is_terminal || (edge.val == "letter" && double_traverse && curr.content.size() == 2)){
      if(double_traverse && curr.content.size() == 2){
        ir.createNode(curr, edge.val + digitEdge.val);
        rule_node_ptr = digitEdge.to;
      }
      else{
        if(edge.val != "") ir.createNode(curr, edge.val);
        rule_node_ptr = edge.to;
      }

      if(edge.val != ""){
        ir.pointToParent();
        input_ptr++;
      }
    }
    else {
      ir.createNode(curr, edge.val);
      stack.push(StackElement(rule_ptr, rule_node_ptr, edge.to, edge.val, edge.is_terminal));
      rule_ptr = grammar.rule_index_for[edge.val];
      rule_node_ptr = 0;
    }

    if(DEBUG) printf("Traversed to %d %d\n", rule_ptr, rule_node_ptr);

    // return from subroutine calls
    while(rule_node_ptr == grammar.rules[rule_ptr].edges.size() - 1){
      StackElement t = stack.front();
      if(DEBUG) printf(" removed from stack rule %d from %d to %d val %s terminal %s\n", t.rule_ptr, t.from, t.to, t.val.c_str(), t.is_terminal ? "T" : "NT");
      ir.pointToParent();
      rule_ptr = t.rule_ptr;
      rule_node_ptr = t.to;
      stack.pop();
    }
  }

  return errors.size() == 0;
}

void SintaticAnalyser::print_tree(){
  ir.print();
}