Report at:
https://sites.google.com/view/2019pcs3866858697


Source code at:
https://bitbucket.org/rsilva77/2019_pcs3866/src


How to compile source
`reset && g++ src/main.cpp src/grammar_parser.cpp src/lexical_analyser.cpp src/rule.cpp src/sintatic_analyser.cpp src/ir_tree.cpp src/code_generator.cpp -Iinclude -o dist/main`


Running the compiler
`reset && dist/main src/basic_grammar.wirth test/a.basic > test/program`


Running the executable
`node src/machine_simulator.js test/program`
