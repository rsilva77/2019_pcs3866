#ifndef GRAMMAR_PARSER_H
#define GRAMMAR_PARSER_H

#include "config.h"
#include "edge_type.h"
#include "rule.h"
#include "token.h"

#include <string>
#include <vector>
#include <map>
#include <set>

using namespace std;

typedef vector <Token> sequenceOfTerminals;

const int _DFS_DEPTH_LIMIT = 20;

class GrammarParser {
  int _dfs_depth;
  string filename;

  static bool is_wirth_control(char &c);
  static bool is_white(char &c);

  void fill_rule_index_for();
  bool raise_if_left_recursion_present();
  void fill_follow();

  set <sequenceOfTerminals> dfs(int rule_idx, int rule_node, int lookahead, int max_loohakead, vector <int> back_rule_idx, vector <int> back_rule_node);
public:
  vector <string> errors;

  vector <Rule> rules;
  map <string, int> rule_index_for;

  set <sequenceOfTerminals> follow[MAX_FOLLOW][MAX_RULES];

  int grammar_lookahead; // max number of lookahed symbols needed

  GrammarParser(string f) : filename(f) {};
  bool parse();

  void print_all();
};

#endif