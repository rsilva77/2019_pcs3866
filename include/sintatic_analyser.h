#ifndef SINTATIC_ANALYSER_H
#define SINTATIC_ANALYSER_H

#include "token.h"
#include "edge_type.h"
#include "rule.h"
#include "grammar_parser.h"
#include "ir_tree.h"

using namespace std;

typedef struct StackElement {
  int rule_ptr, from, to;
  string val;
  bool is_terminal;
  StackElement() {}
  StackElement(int rule_ptr, int from, int to, string val, bool is_terminal) : rule_ptr(rule_ptr), from(from), to(to), val(val), is_terminal(is_terminal) {};
} StackElement;

class SintaticAnalyser {
  GrammarParser grammar;
  vector <Token> input;

  int rule_ptr, rule_node_ptr, input_ptr;

  bool double_traverse;
  edgeType digitEdge;

  bool canTraverseTerminalEdgeAsLetterDigit(Token a, edgeType edge);
  bool canTraverseNonTerminalEdgeAsLetterDigit(Token a, edgeType edge);
  bool canTraverse(Token a, edgeType edge, int lookahead);
  vector <int> getPossibleEdges();
public:
  IRTree ir;
  vector <string> errors;

  SintaticAnalyser(GrammarParser g, vector <Token> i) : grammar(g), input(i) {};
  bool process();
  void print_tree();
};

#endif