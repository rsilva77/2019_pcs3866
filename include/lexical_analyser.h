#ifndef LEXICAL_ANALYSER_H
#define LEXICAL_ANALYSER_H

#include "token.h"

#include <string>
#include <vector>

using namespace std;

class LexicalAnalyser {
  string filename;
  string input;

  bool is_control(char x);
  bool is_digit(char x);
  bool is_letter(char x);
  bool is_digit_or_letter(char x);
  bool is_plus_or_minus(char x);
  bool is_math_operator(char x);
  bool is_white(char x);
  bool is_doublequote(char x);

  string process_identifier(int pos);
  string process_number(int pos);
  string process_rawstring(int pos);
public:
  vector <string> errors;
  vector <Token> output;

  LexicalAnalyser(string _input);
  bool process();

  void print_output();
  void print_errors();
};

#endif