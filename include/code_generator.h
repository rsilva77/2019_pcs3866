#ifndef CODE_GENERATOR_H
#define CODE_GENERATOR_H

#include "token.h"
#include "ir_tree.h"
#include "stack.h"
#include "config.h"

#include <string>
#include <vector>
#include <map>

using namespace std;

typedef vector <string> Code;

typedef vector <int> Dimensions;

typedef struct ForContextElement {
  string for_variable, step_hashcode, comparison_hashcode, next_hashcode;
  ForContextElement () {}
  ForContextElement(string a, string b, string c, string d) : for_variable(a), step_hashcode(b), comparison_hashcode(c), next_hashcode(d) {}
} ForContextElement;

class CodeGenerator {
  IRTree ir;
  map <int, int> label_to_instruction_address;

  map <string, int> variable_to_address;
  map <string, Dimensions> variable_to_dims;
  map <string, int> variable_to_size;
  int last_variable_address;

  map <string, int> function_to_label;
  map <string, string> function_to_parameter;

  Stack <ForContextElement> for_stack;
  map <string, string> hashcode_to_label;
  map <string, int> hashcode_to_from_expression_size, hashcode_to_step_expression_size;

  Code solve_program(int node_idx);
  Code solve_bstatement(int node_idx);
  Code solve_assign(int node_idx);
  Code solve_var(int node_idx);
  Code solve_exp(int node_idx);
  Code solve_eb(int node_idx);
  Code solve_predef(int node_idx);
  Code solve_read(int node_idx);
  Code solve_data(int node_idx);
  Code solve_print(int node_idx);
  Code solve_pitem(int node_idx);
  Code solve_goto(int node_idx);
  Code solve_if(int node_idx);
  Code solve_for(int node_idx);
  Code solve_next(int node_idx);
  Code solve_dim(int node_idx);
  Code solve_def(int node_idx);
  Code solve_gosub(int node_idx);
  Code solve_return(int node_idx);
  Code solve_remark(int node_idx);
  Code solve_end(int node_idx);

  map <string, int> operation_to_op_code;

  string create_hashcode(string base);
public:
  vector <string> errors;
  vector <string> code;

  Code solve(int node_idx);

  CodeGenerator(IRTree _ir);
  Code generate();
};

#endif