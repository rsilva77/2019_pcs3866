#ifndef CONFIG_H
#define CONFIG_H

#define HANDLE_RULE_ERRORS
// 3: look at current symbol and next one. 4: takes too long to compile
#define MAX_FOLLOW 3
#define DFS_MAX_DEPTH 1000
#define MAX_RULES 300
#define DEBUG 0

#endif