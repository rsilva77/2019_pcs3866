#ifndef IR_TREE_H
#define IR_TREE_H

#include <string>
#include <vector>

#include "edge_type.h"
#include "token.h"

using namespace std;

typedef struct IRTreeNode{
  vector <int> sons;
  Token t;
  string edge_val;
  IRTreeNode() {}
  IRTreeNode(Token _t, string _edge_val) : t(_t), edge_val(_edge_val) {}
} IRTreeNode;

class IRTree {
public:
  int current_node;
  vector <IRTreeNode> nodes;
  vector <int> T; // parent of i-th node
  IRTree();
  void createNode(Token t, string edge_val);
  void pointToParent();

  void print();
};

#endif