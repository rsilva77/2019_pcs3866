#ifndef STACK_H
#define STACK_H

template <typename StackElement>
class Stack {
public:
  int top;
  vector <StackElement> v;

  Stack(){ top = 0; v.clear(); }

  void push(StackElement e){
    if(top >= v.size()) v.push_back(e);
    else v[top] = e;
    top++;
  }

  void pop(){ top--; }

  StackElement front(){
    if(top) return v[top-1];
    throw "Peeking empty stack.";
  }
};

#endif