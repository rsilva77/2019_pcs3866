#ifndef EDGE_TYPE_H
#define EDGE_TYPE_H

#include <string>

using namespace std;

typedef struct edgeType{
  int to;
  string val;
  bool is_terminal;
  edgeType() {}
  edgeType(int a, string b, bool c) : to(a), val(b), is_terminal(c) {}
} edgeType;

#endif