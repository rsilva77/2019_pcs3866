#ifndef TOKEN_H
#define TOKEN_H

#include <string>

using namespace std;

typedef struct Token{
  string content;
  string classification;
  Token() {}
  Token(string a) { content = a; classification = ""; }
  Token(string a, string b){ classification = a; content = b; }
  bool operator < (const Token &a) const {
    return classification < a.classification ||
           (classification == a.classification && content < a.content);
  }
  bool same_as(const Token &a) { return classification == a.classification; }
  string to_edge_val(){
    if(classification == "int") return "int";
    if(classification == "num") return "int";
    if(classification == "rawstring") return "rawstring";
    return content;
  }
} Token;

#endif