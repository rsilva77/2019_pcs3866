#ifndef RULE_H
#define RULE_H

#include <string>
#include <vector>
#include <utility>

#include "edge_type.h"
#include "stack.h"

using namespace std;

typedef struct ControlElement {
  char control;
  int automaton_node;
  int position;
} ControlElement;

typedef struct EndOfOptionElement {
  int automaton_node_begin, automaton_node_end;
  int position;
} EndOfOptionElement;

class Rule {
  int stack_top;
  Stack <ControlElement> control_stack;

  Stack <EndOfOptionElement> end_of_option_stack;

  int current_node, current_position;

  static char err_buff[300]; // static saves us memory while disabling parallelism

  int create_node(); // return id of created node
  bool create_edge(int from, int to, string s, bool is_terminal);

  bool add_terminal(const string &s);
  bool add_nonterminal(const string &s);

  static void upcase(string &s);

public:
  string lval;
  vector< vector<edgeType> > edges;

  vector <string> errors;

  Rule();
  bool add_element(const string &terminal, const string &nonterminal);
  bool add_lval(string s);
  bool add_control(char c);

  void print_graph();
};

#endif